<?php

// array for JSON response
$response = array();
 
// check for required fields
if (isset($_GET['studNum']) && isset($_GET['password'])) {
	
// include db connect class
require_once __DIR__ . '/db_connect.php';

// connecting to db
$db = new DB_CONNECT();

// username and password sent from form 
$studNum=$_GET['studNum']; 
$password=$_GET['password'];

// To protect MySQL injection (more detail about MySQL injection)
$studNum = stripslashes($studNum);
$password = stripslashes($password);
$studNum = mysql_real_escape_string($studNum);
$password = mysql_real_escape_string($password);

$result = mysql_query("SELECT * FROM login WHERE student_number='$studNum' and password='$password'");
$num_rows = mysql_num_rows($result);

    // check if row inserted or not
    if ($num_rows >= 1) {
        // successfully inserted into database
        $response["status"] = "success";	
		while($row = mysql_fetch_array($result))
		  {
		  	$studNum = $row["student_number"];
			$password = $row["password"];
			$response["params"] = array('studNum' => $studNum, 'password' => $password);
		  }
 
        // echoing JSON response
        echo json_encode($response);
    } else {
        // failed to insert row
        $response["status"] = "failed";
        $response["message"] = "Oops! An error occurred.";
 
        // echoing JSON response
        echo json_encode($response);
    }
} else {
    // required field is missing
    $response["status"] = "failed";
    $response["message"] = "Required field(s) is missing";
 
    // echoing JSON response
    echo json_encode($response);
}
?>