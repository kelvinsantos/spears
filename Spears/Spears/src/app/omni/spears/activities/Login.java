package app.omni.spears.activities;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import app.omni.spear.news.News;
import app.omni.spears.R;
import app.omni.spears.managers.*;

public class Login extends Activity{
	Button btnLogin;
	LoginDataBaseAdapter loginDataBaseAdapter;

	// Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();
	
	// Profile JSON url
    private static final String PROFILE_URL = "http://192.168.1.101/android_connect/checklogin.php";
    
	@Override
	protected void onCreate(Bundle savedInstanceState){
	     super.onCreate(savedInstanceState);
	     setContentView(R.layout.activity_login);

	     btnLogin=(Button)findViewById(R.id.btnLogin);

			// Set On ClickListener
			btnLogin.setOnClickListener(new View.OnClickListener() {

				public void onClick(View v) {
				     EditText txtStudNum=(EditText) findViewById(R.id.txtStudNum);
				     EditText txtPassword=(EditText) findViewById(R.id.txtPassword);
					// get The User name and Password
					String studNum = txtStudNum.getText().toString();
					String password = txtPassword.getText().toString();
					
					List<NameValuePair> params = new ArrayList<NameValuePair>();
			        params.add(new BasicNameValuePair("studNum", studNum));
			        params.add(new BasicNameValuePair("password", password));

			        try {
			        	
				        // getting JSON string from URL
				        JSONObject json = jsonParser.makeHttpRequest(PROFILE_URL, "POST", params);

				        // Check your log cat for JSON reponse
				        Log.d("Profile JSON: ", json.toString());

			        	String status = json.getString("status");
			        	if (status != null && status.equals("success")) {
			        		
			        		JSONObject items = new JSONObject(json.getString("params"));
			        		String dbStudNum = items.getString("studNum");
			        		String dbPassword = items.getString("password");
			        		
			        		// check if the Stored password matches with  Password entered by user
							if(studNum != null && studNum.equals(dbStudNum) && password != null && password.equals(dbPassword))
							{
								 Intent i = new Intent(getApplicationContext(), Main.class);
			                     startActivity(i);
			                     finish();
							}
							else
							{
								Toast.makeText(Login.this, "User Name or Password does not match", Toast.LENGTH_LONG).show();
							}
			        	}

			        } catch (JSONException e) {
			            e.printStackTrace();
			        }
				}
			});

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	    // Close The Database
		loginDataBaseAdapter.close();
	}
    
	private long lastPressedTime;
	private static final int PERIOD = 3000;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
	        switch (event.getAction()) {
	        case KeyEvent.ACTION_DOWN:
	            if (event.getDownTime() - lastPressedTime < PERIOD) {
	                finish();
	            } else {
	                Toast.makeText(getApplicationContext(), "Press again to exit.",
	                        Toast.LENGTH_SHORT).show();
	                lastPressedTime = event.getEventTime();
	            }
	            return true;
	        }
	    }
	    return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

}
