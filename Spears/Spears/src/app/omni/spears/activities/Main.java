package app.omni.spears.activities;

import android.os.Bundle;
import android.app.Activity;
import android.app.TabActivity;
import android.content.Intent;
import android.view.Menu;
import android.widget.TabHost;
import app.omni.spears.R;
import app.omni.spears.tab.*;


public class Main extends TabActivity{

		@Override
        public void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                
                TabHost host = getTabHost();

        	    host.addTab(host.newTabSpec("one").setIndicator("Forum")
        	        .setContent(new Intent(this, Forum.class)));
        	    host.addTab(host.newTabSpec("two").setIndicator("Latest Threads")
        	        .setContent(new Intent(this, LatestThreads.class)));
        	    host.addTab(host.newTabSpec("three").setIndicator("Profile")
        		        .setContent(new Intent(this, Profile.class)));
           
        }

	   
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
