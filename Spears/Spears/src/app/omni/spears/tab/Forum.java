package app.omni.spears.tab;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import app.omni.spear.news.News;
import app.omni.spears.R;

public class Forum extends ListActivity {
 
	static final String[] FORUM = new String[] { "News", "Announcements", "General Discussion", "Problem Section", "Spam Area" };
		
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
 
		// no more this
		// setContentView(R.layout.activity_forum);
 
		setListAdapter(new ArrayAdapter<String>(this, R.layout.activity_forum,FORUM));
 
		ListView listView = getListView();
		listView.setTextFilterEnabled(true);
 
		listView.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
			    // When clicked, show a toast with the TextView text
				Intent i = new Intent(getApplicationContext(), News.class);
                startActivity(i);
                finish();
			}
		});
 
	}
	
    
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.forum, menu);
		return true;
	}

}
