package app.omni.spears;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import app.omni.spears.activities.Login;


public class RulesAndRegulations extends Activity {

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rules_and_regulations);
		
		toLogin();
	}
	
        
	public void toLogin() {
		Button toLogin = (Button) findViewById(R.id.btnContinue);
		toLogin.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = new Intent(RulesAndRegulations.this, Login.class);
                startActivity(i);
                finish();
			}
		});
	}
	

	private long lastPressedTime;
	private static final int PERIOD = 3000;

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
	    if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
	        switch (event.getAction()) {
	        case KeyEvent.ACTION_DOWN:
	            if (event.getDownTime() - lastPressedTime < PERIOD) {
	                finish();
	            } else {
	                Toast.makeText(getApplicationContext(), "Press again to exit.",
	                        Toast.LENGTH_SHORT).show();
	                lastPressedTime = event.getEventTime();
	            }
	            return true;
	        }
	    }
	    return false;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.rules_and_regulations, menu);
		return true;
	}

}
