package app.omni.spear.news;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import app.omni.spears.R;
import app.omni.spears.managers.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class showNews extends Activity {
 
	// All xml labels

    TextView title;
    TextView date;
    TextView text;

    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
    JSONParser jsonParser = new JSONParser();

    // Profile json object
    JSONArray user;
    JSONObject hay;
    // Profile JSON url
    private static final String PROFILE_URL = "http://10.0.2.2/android_connect/get_post.php";

    // ALL JSON node names
    private static final String TAG_TITLE = "title";
    private static final String TAG_DATE = "created_at";
    private static final String TAG_TEXT = "text";

    @Override
    public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.show_news);


    title = (TextView) findViewById(R.id.txtTitle);
    date = (TextView) findViewById(R.id.txtDate);
    text = (TextView) findViewById(R.id.txtText);

    // Loading Profile in Background Thread
    new LoadProfile().execute();
    }

    /**
    * Background Async Task to Load profile by making HTTP Request
    * */
    class LoadProfile extends AsyncTask<String, String, String> {

    /**
     * Before starting background thread Show Progress Dialog
     * */

    public void test(){

                /**
                 * Updating parsed JSON data into ListView
                 * */
                // Storing each json item in variable
                try {
                    String txtTitle = hay.getString(TAG_TITLE);
                    String created_at = hay.getString(TAG_DATE);
                    String txtText = hay.getString(TAG_TEXT);

                    // displaying all data in textview

                    title.setText(hay.getString(TAG_TITLE));
                    date.setText(hay.getString(TAG_DATE));
                    text.setText(hay.getString(TAG_TEXT));

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(showNews.this);
        pDialog.setMessage("Loading profile ...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }
    /**
     * getting Profile JSON
     * */
    protected String doInBackground(String... args) {
        // Building Parameters

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(showNews.this);
        String post_title = sp.getString("title", "anon");
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("title", post_title));
        // getting JSON string from URL
        JSONObject json = jsonParser.makeHttpRequest(PROFILE_URL, "POST",
                params);

        // Check your log cat for JSON reponse
        Log.d("Profile JSON: ", json.toString());

        try {
            // profile json object
            user = json.getJSONArray(TAG_TITLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * After completing background task Dismiss the progress dialog
     * **/
    protected void onPostExecute(String file_url) {
        // dismiss the dialog after getting all products
        pDialog.dismiss();
        // updating UI from Background Thread
        test();

    }

}
    }